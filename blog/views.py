from django.shortcuts import render

# Create your views here.


def show_index(request):
    title = "Santosh JSG"
    return render(request, "index.html", {'title': title})

